from django.conf import settings
from django.db import models

# Create your models here.
class Pizza(models.Model):
    dough = models.BooleanField()
    meat = models.ForeignKey('Meat', on_delete=models.CASCADE)
    chicken = models.ForeignKey('Chicken', on_delete=models.CASCADE)
    other = models.ForeignKey('Other', on_delete=models.CASCADE)


class Meat(models.Model):
    pork = models.PositiveIntegerField
    salami = models.PositiveIntegerField
    mutton = models.PositiveIntegerField


class Chicken(models.Model):
    pass


class Other(models.Model):
    pass

from django.apps import AppConfig


class CustomPizzaConfig(AppConfig):
    name = 'custom_pizza'
